const captureWebsite = require('capture-website');
const fs = require('fs');
const looksSame = require('looks-same');
const png = require('looks-same/lib/png');
const git = require('simple-git/promise');
const open = require('open');
const Color = require('color');
const httpServer = require('http-server');
const Promise = require('bluebird');

const config = require('./run-config');

const createReport = require('./src/createReport');

const defaultCaptureOptions = {
  width: 1920,
  height: 1000,
  fullPage: true,
  delay: 3,
  overwrite: true,
  hideElements: ['.fa-spinner', '#js-peek', '.js-timeago', '.updated-note'],
  removeElements: ['#js-peek'],
  modules: [
    `
    document.documentElement.classList.remove('with-performance-bar')
    `,
  ],
  launchOptions: {
    args: ['--no-sandbox', '--disable-setuid-sandbox'],
  },
};

let isGoldenMaster = false;
let onlyComparison = false;

// Check command line arguments
process.argv.forEach(val => {
  if (val === '--gold' || val === '-G') {
    isGoldenMaster = true;
  } else if (val === '--compare' || val === '-C') {
    onlyComparison = true;
  }
});

// Create capture array
let captures = [];
config.urls.map(url => {
  if (config.devices) {
    let deviceCaptures = config.devices.map(device => {
      return {
        device,
        url,
      };
    });
    captures = captures.concat(deviceCaptures);
  }
  captures.push({ device: 'desktop', url });
});

captures.forEach(capture => {
  capture.fullUrl = config.baseUrl + capture.url;

  capture.isDifferent = false;
  capture.differences = null;

  capture.urlToPath = capture.url.replace('/', '').replace(/\//gi, '-');
  if (capture.device) {
    capture.urlToPath += `-${capture.device.replace(/ /gi, '')}`;
  }
});

(async () => {
  let gitBranchName = 'master';
  try {
    const gitlabGit = git(config.pathToRepo);
    const statusSummary = await gitlabGit.status();
    if (statusSummary && statusSummary.current) {
      gitBranchName = statusSummary.current;
      console.log(`Detected Branch "${gitBranchName}" in "${config.pathToRepo}"`);
    }
  } catch (e) {
    console.error('Error with Git Status : ', e);
  }

  let baseCaptureDir = `./captures/${gitBranchName}`;
  let actualCaptureDir = isGoldenMaster ? '/golden' : '/testrun';
  fs.mkdirSync(baseCaptureDir + actualCaptureDir, { recursive: true });
  fs.mkdirSync(baseCaptureDir + '/diff', { recursive: true });

  if (!onlyComparison) {
    console.log('\nStarting taking ' + captures.length + ' Screenshots\n');
    await Promise.map(
      captures,
      capture => {
        console.log('Capturing ' + capture.url + ' on ' + capture.device);

        const captureUrl = config.baseUrl + capture.url;
        const screenshotPath = `${baseCaptureDir + actualCaptureDir}/${capture.urlToPath}.png`;

        const captureOptions = { ...defaultCaptureOptions };
        if (capture.device != 'desktop') {
          captureOptions.emulateDevice = capture.device;
        }
        return captureWebsite.file(captureUrl, screenshotPath, captureOptions);
      },
      { concurrency: 2 }
    );

    console.log('\nFinished ' + captures.length + ' Screenshots');
  }

  if (!isGoldenMaster) {
    console.log('\nStarting Comparison ...\n');

    await Promise.map(
      captures,
      capture => {
        return new Promise((resolve, reject) => {
          const goldenScreenshotPath = `${baseCaptureDir}/golden/${capture.urlToPath}.png`;
          const testScreenshotPath = `${baseCaptureDir}/testrun/${capture.urlToPath}.png`;
          const diffPath = `${baseCaptureDir}/diff/${capture.urlToPath}.png`;

          const diffOptions = {
            stopOnFirstFail: false,
            reference: goldenScreenshotPath,
            current: testScreenshotPath,
            highlightColor: '#ff0000', // color to highlight the differences
            strict: false, // strict comparsion
            tolerance: 2.5,
            antialiasingTolerance: 0,
            ignoreAntialiasing: true, // ignore antialising by default
            ignoreCaret: true, // ignore caret by default
          };

          looksSame(goldenScreenshotPath, testScreenshotPath, diffOptions, (error, result) => {
            if (error) {
              console.error('Error on comparison : ', error);
              reject(error);
            }
            const { equal, diffBounds, diffClusters } = result;
            if (!equal) {
              console.log(
                'Screenshot Mismatch : ' + capture.fullUrl + ' (' + capture.device + ') - ',
                equal,
                diffBounds,
                diffClusters
              );

              capture.isDifferent = true;
              capture.differences = diffClusters;

              looksSame.createDiff(diffOptions, (error, buffer) => {
                if (error) {
                  console.error('Error on diff creation ', error);
                  reject(error);
                }

                png.fromBuffer(buffer, {}, (err, result) => {
                  const highColor = {
                    R: 255,
                    G: 0,
                    B: 0,
                  };

                  iterateRect(
                    result.width,
                    result.height,
                    (x, y) => {
                      const colorRes = result.getPixel(x, y);

                      // if (y === 0) console.log('C : ', colorRes);

                      if (!looksSame.colors(colorRes, highColor)) {
                        let whiteFactor = 0.8;

                        diffClusters.forEach(area => {
                          if (
                            x >= area.left &&
                            x <= area.right &&
                            y >= area.top &&
                            y <= area.bottom
                          ) {
                            whiteFactor = 0;
                          }
                        });

                        let newColor = Color({
                          r: colorRes.R,
                          g: colorRes.G,
                          b: colorRes.B,
                        }).mix(Color('white'), whiteFactor);

                        const newCExp = {
                          R: Math.round(newColor.red()),
                          G: Math.round(newColor.green()),
                          B: Math.round(newColor.blue()),
                        };

                        result.setPixel(x, y, newCExp);
                      }
                    },
                    () => {
                      result.save(diffPath, savedRes => {
                        resolve();
                      });
                    }
                  );
                });
              });
            } else {
              resolve();
            }
          });
        });
      },
      { concurrency: 2 }
    );

    createReport(captures, 'Screenshot diff report for branch ' + gitBranchName, baseCaptureDir);

    if (config.startWebserver) {
      var server = httpServer.createServer({
        root: baseCaptureDir,
        autoIndex: false,
        showDir: false,
        ext: 'htm',
        cache: -1,
      });
      server.listen(8080);

      if (config.startBrowser) {
        await open('http://localhost:8080');
      }

      console.log('Serving web server with report on http://localhost:8080');
    }
  }

  console.log('Done');
})();

const iterateRect = (width, height, callback, endCallback) => {
  const processRow = y => {
    setImmediate(() => {
      for (let x = 0; x < width; x++) {
        callback(x, y);
      }

      y++;

      if (y < height) {
        processRow(y);
      } else {
        endCallback();
      }
    });
  };

  processRow(0);
};
