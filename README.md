# GitLab Screener

A little tool that runs through your local GDK instance, takes screenshots and does visual diffing when you are doing CSS refactoring. Not meant for CI but rather refactoring and checking against the same branch. Right now the default config in `run-config.js` has 30 URL's you can easily setup your own list. It takes screenshot for 3 different devices (desktop + mobile + tablet) this can be also configured easily. 

It will create after the second run an report like this - https://timzallmann.gitlab.io/gitlab-screener/ (Here one color was slightly changed)

### Workflow proposal

1. Clone locally in a directory
1. Check `run-config.js` if the address matches your GDK + path to your gitlab directory (needed to get your current branchname)
1. Reset your local branch to the last commit without your changes
1. Create golden master screenshots `node index.js --gold`
1. Forward your branch to your changes and run the screener again `node index.js`
1. Get a coffee ...
1. The script will create a report in `/captures/[branchName]/` and serve that report locally through an HTTP server.
1. The browser should open when its done and show you an report on http://localhost:8080 like this [example report](https://timzallmann.gitlab.io/gitlab-screener/)

### Additional infos

`node index.js --compare` To only run the comparison but not the screenshot making.

The script is actually removing timeago labels, antialiasing, carets etc. to minimize false positives.

