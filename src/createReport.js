const reporting = require('vue-reports/reporting');

function createVueReport(captures, fullTitle, savePath) {
  const report = new reporting.Report(fullTitle);

  captures.sort((captureA, captureB) => {
    return captureB.isDifferent || captureA.isDifferent;
  });

  captures.forEach(capture => {
    const lb = new reporting.ImageDiffBlock(
      capture.url + ' on ' + capture.device,
      capture.fullUrl,
      `golden/${capture.urlToPath}.png`,
      `testrun/${capture.urlToPath}.png`,
      capture.isDifferent ? `diff/${capture.urlToPath}.png` : '',
      capture.differences
    );
    report.addBlock(lb);
  });

  reporting.ReportSaver.saveReport(report, savePath);
}

module.exports = createVueReport;
